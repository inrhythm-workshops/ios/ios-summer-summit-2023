//
//  SettingsView.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/15/23.
//

import SwiftUI

struct SettingsView: View {
    
    @State var settings: [Settings:Bool] = [.a: true, .b: false, .c: true]
    
    var body: some View {
        List {
            ForEach(Settings.allCases) { setting in
                NavigationLink(value: setting) {
                    HStack {
                        Text(setting.title)
                        Spacer()
                        Text(settings[setting] ?? false ? "On" : "Off")
                            .foregroundColor(.secondary)
                    }
                }
            }
        }
        .navigationTitle("Settings")
        .navigationDestination(for: Settings.self) { setting in
            let binding = Binding {
                settings[setting] ?? false
            } set: { value, _ in
                settings[setting] = value
            }
            SettingsDetailView(title: setting.title, isOn: binding)
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
