//
//  SettingsDetailView.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/15/23.
//

import SwiftUI

struct SettingsDetailView: View {
    let title: String
    @Binding var isOn: Bool
    var body: some View {
        List {
            Toggle(title, isOn: $isOn)
        }
        .navigationTitle("Setting")
        .navigationBarTitleDisplayMode(.inline)
    }
}
