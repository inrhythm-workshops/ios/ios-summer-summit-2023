//
//  Setting.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/15/23.
//

import Foundation

enum Settings: String, Identifiable, Hashable, CaseIterable {
    
    case a
    case b
    case c
    
    var id: String { self.rawValue }
    
    var title: String {
        "Option \(self.rawValue.uppercased())"
    }
    
}
