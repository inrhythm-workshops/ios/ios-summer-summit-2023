//
//  AccountsView.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/20/23.
//

import Combine
import SwiftUI

struct AccountsListView: View {
    
    @StateObject var model = AccountsListViewModel()
    
    @EnvironmentObject var appTabViewModel: AppTabViewModel
    
    var body: some View {
        Group {
            if model.accounts.isEmpty {
                ProgressView("Loading")
                    .task {
                        await model.load()
                    }
            } else {
                List {
                    ForEach(model.accounts) { account in
                        NavigationLink(account.name, value: account)
                    }
                }
            }
        }
        .navigationDestination(for: Account.self) { account in
            AccountDetailsView(account: account)
        }
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button("Settings A") {
                    appTabViewModel.dispatch(route: .init(tab: .settings, value: Settings.a))
                }
            }
        })
        .navigationTitle("Accounts")
    }
    
}
