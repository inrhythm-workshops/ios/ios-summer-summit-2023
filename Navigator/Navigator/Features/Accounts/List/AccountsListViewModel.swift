//
//  AccountsViewModel.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/15/23.
//

import Foundation

class AccountsListViewModel: ObservableObject {
    
    @Published var accounts: [Account] = []
    @Published var selectedAccount: Account?
    
    private let manager: AccountManager = .shared
    
    func load() async {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.accounts = self.manager.accounts
        }
    }
    
}
