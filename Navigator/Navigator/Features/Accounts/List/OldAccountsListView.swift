//
//  AccountsView.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/13/23.
//

import SwiftUI

/// Old accounts view that uses the traditional NavigationLink(_title:destination) method of handling items in a list.

struct OldAccountsView: View {
    
    @StateObject var model = AccountsListViewModel()
    
    var body: some View {
        NavigationView {
            List {
                ForEach(model.accounts) { account in
                    NavigationLink(account.name) {
                        Text(account.name)
                    }
                }
            }
            .navigationTitle("Accounts")
            .task {
                await model.load()
            }
        }
    }
    
}

struct AccountsView_Previews: PreviewProvider {
    static var previews: some View {
        OldAccountsView()
    }
}
