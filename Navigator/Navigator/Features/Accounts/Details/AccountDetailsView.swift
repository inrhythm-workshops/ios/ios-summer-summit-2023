//
//  AccountDetailView.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/15/23.
//

import SwiftUI

struct AccountDetailsView: View {
    
    let account: Account
    
    @EnvironmentObject var router: NavigationStackRouter
    
    var body: some View {
        List {
            Section {
                NameValueView(name: "Account", value: account.name)
                NameValueView(name: "Balance", value: account.formattedBalance)
                NameValueView(name: "Type", value: account.type)
                NameValueView(name: "ID", value: "\(account.id)")
            }
            Section {
                NameValueView(name: "Options", value: "Allowed")
                NameValueView(name: "Trading", value: "Restricted")
            }
            Section {
                NavigationLink("Legal Information", value: AccountLegalViewLink(legal: "The Fine Print"))
                    .foregroundColor(.secondary)
                
                Button("Programatic Legal Information") {
                    router.append(AccountLegalViewLink(legal: "The Fine Print"))
                }
            }
        }
        .navigationTitle("Account")
        .navigationBarTitleDisplayMode(.inline)
        .navigationDestination(for: AccountLegalViewLink.self) { link in
            AccountLegalView(link: link)
        }
        
    }
}

private struct NameValueView: View {
    let name: String
    let value: String
    var body: some View {
        HStack {
            Text(name)
            Spacer()
            Text(value)
                .foregroundColor(.secondary)
        }
    }
}


