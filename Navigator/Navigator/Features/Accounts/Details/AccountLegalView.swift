//
//  AccountLegalView.swift
//  DeepLinking
//
//  Created by Long, Hoy on 6/15/23.
//

import SwiftUI

struct AccountLegalViewLink: Hashable {
    let legal: String
}

struct AccountLegalView: View {
    
    let link: AccountLegalViewLink
    
    @EnvironmentObject var router: NavigationStackRouter
    
    var body: some View {
        List {
            Text(link.legal)
            Text("This is the fine print. Make sure you read this otherwise you may owe us your firstborn child.")
                .foregroundColor(.secondary)
            Button("Done") {
                router.pop()
            }
        }
        .navigationTitle("Legal")
        .navigationBarTitleDisplayMode(.inline)
    }
}
