//
//  AppTabView.swift
//  Navigator
//
//  Created by Long, Hoy on 9/25/23.
//

import SwiftUI

/// List of application tabs
enum AppTabs: String, Hashable, CaseIterable {
    case accounts
    case settings
}

/// Main application tab view.
struct AppTabView: View {
    
    @StateObject var model = AppTabViewModel()
    
    var body: some View {
        TabView(selection: $model.selectedTab) {
            AccountsListView()
                .addNavigationStack(router: model.router(for: .accounts))
                .tabItem {
                    Label("Accounts", systemImage: "list.dash")
                }
                .tag(AppTabs.accounts)
            
            SettingsView()
                .addNavigationStack(router: model.router(for: .settings))
                .tabItem {
                    Label("Settings", systemImage: "gear")
                }
                .tag(AppTabs.settings)
        }
        .environmentObject(model)
        .onOpenURL { url in
            model.handleOpenURL(url: url)
        }
    }
    
}


struct AppTabView_Previews: PreviewProvider {
    static var previews: some View {
        AppTabView()
    }
}
