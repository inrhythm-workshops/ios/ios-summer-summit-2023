//
//  AppRoute.swift
//  Navigator
//
//  Created by Long, Hoy on 9/25/23.
//

import SwiftUI

/// Application route specifying destination tab and path.
struct AppRoute {
    
    /// The destination tab
    let tab: AppTabs
    /// The destination path
    let path: NavigationPath?
    
    /// Default initializers
    init(tab: AppTabs) {
        self.tab = tab
        self.path = nil
    }
    
    init(tab: AppTabs, path: NavigationPath) {
        self.tab = tab
        self.path = path
    }
    
    init<T:Hashable>(tab: AppTabs, value: T) {
        self.tab = tab
        self.path = .init([value])
    }
    
}
