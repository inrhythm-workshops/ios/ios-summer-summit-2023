//
//  AppTabViewModel.swift
//  Navigator
//
//  Created by Long, Hoy on 9/25/23.
//

import SwiftUI

/// ApplicationTabViewModel manages the selected tab and each tab's NavigationStackRouter.
///
/// It also manages URL routing and destination dispatching for the application.
class AppTabViewModel: ObservableObject {
    
    @Published var selectedTab: AppTabs = .accounts
    
    /// List of URL Handlers used to resolve routes
    private var handlers: [any URLHandling] = [
        AccountsURLHandler(),
        SettingsURLHandler()
    ]
    
    /// Internal dictionary of NavigationStackRouters, one for each tab.
    private lazy var navigationStackRouters: [AppTabs: NavigationStackRouter] =
        Dictionary(uniqueKeysWithValues: AppTabs.allCases.map { ($0, NavigationStackRouter()) })

    
    /// Primary URL handling mechanism, walks URL handlers looking for a match.
    @MainActor
    func handleOpenURL(url: URL) {
        for handler in handlers {
            if let route = handler.handles(url: url) {
                dispatch(route: route)
                break
            }
        }
    }
    
    /// Primary dispatch mechanism, used by handleOpenURL and others.
    @MainActor
    func dispatch(route: AppRoute) {
        selectedTab = route.tab
        if let path = route.path {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.router(for: route.tab).update(path: path)
            }
        }
    }
    
    /// Convenience function to get a NavigationStackRouter for a specific tab. Avoids unwrapping.
    func router(for tab: AppTabs) -> NavigationStackRouter {
        // Since the router list built with AppTabs.allCases it will always have what we
        // need, but as result is optional and we can't bind to an optional
        // we need to explicitly unwrap it.
        navigationStackRouters[tab]!
    }
    
}
