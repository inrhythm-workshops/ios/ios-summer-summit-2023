//
//  URLHandling.swift
//  Navigator
//
//  Created by Long, Hoy on 9/25/23.
//

import SwiftUI

/// Protocol for URLHandlers
protocol URLHandling {
    func handles(url: URL) -> AppRoute?
}

/// Specific instance for Accounts URLHandling
///
/// xcrun simctl openurl booted "navigator:/accounts"
/// xcrun simctl openurl booted "navigator:/accounts/222"
///
struct AccountsURLHandler: URLHandling {
    func handles(url: URL) -> AppRoute? {
        if url.path().hasPrefix("/accounts") {
            if let account = AccountManager.shared.account(for: url.lastPathComponent) {
                return .init(tab: .accounts, value: account)
            }
            return .init(tab: .accounts)
        }
        return nil
    }
}

/// Specific instance for Settings URLHandling
///
/// xcrun simctl openurl booted "navigator:/settings"
///
struct SettingsURLHandler: URLHandling {
    func handles(url: URL) -> AppRoute? {
        if url.path().hasPrefix("/settings") {
            return .init(tab: .settings)
        }
        return nil
    }
}
