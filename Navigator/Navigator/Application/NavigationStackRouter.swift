//
//  NavigationStackRouter.swift
//  Navigator
//
//  Created by Long, Hoy on 9/25/23.
//

import SwiftUI

/// Primary observable container for NavigationPaths.
class NavigationStackRouter: ObservableObject {
    
    @Published var path: NavigationPath = .init()
    
    func update(path: NavigationPath) {
        self.path = path
    }
    
    func append<T:Hashable>(_ value: T) {
        path.append(value)
    }
    
    func pop() {
        path.removeLast(1)
    }
    
    func reset() {
        path.removeLast(path.count)
    }

}

extension View {
    /// Use this function instead of NavigationStackRouterView if that's more convenient.
    ///
    /// Both default to the functionality in NavigationStackRouterSupport.
    func addNavigationStack(router: NavigationStackRouter) -> some View {
        self.modifier(NavigationStackRouterSupport(router: router))
    }
}

struct NavigationStackRouterSupport: ViewModifier {
    @ObservedObject public var router: NavigationStackRouter
    func body(content: Content) -> some View {
        NavigationStack(path: $router.path) {
            content
        }
        .environmentObject(router)
    }
}
