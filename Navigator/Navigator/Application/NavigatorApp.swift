//
//  NavigatorApp.swift
//  Navigator
//
//  Created by Long, Hoy on 9/25/23.
//

import SwiftUI

@main
struct NavigatorApp: App {
    var body: some Scene {
        WindowGroup {
            AppTabView()
        }
    }
}
