Pod::Spec.new do |spec|
    spec.name          = 'EcommerceApp'
    spec.version       = '1.0.0'
    spec.summary       = 'This is Ecommerce App'
    spec.license       = { :type => 'MIT', :file => 'License.md'}
    spec.homepage      = ' '
    spec.source        = { :git => ' ', :tag => spec.version, :path => '/' }
    spec.author = {'Mahmood, Hamid' => 'hamid.mahmood@newbuffers.com'}
    spec.module_name   = 'EcommerceApp'
    spec.ios.deployment_target  = '15.0'
    spec.watchos.deployment_target  = '6'
    spec.swift_version = '5'
    spec.source_files       = 'EcommerceApp/**/*.swift'
    spec.resources = [
      'EcommerceApp/*.strings',
      'EcommerceApp/*.{xcassets}',
    ]
    spec.dependency 'Authentication'
    spec.dependency 'ProductManagement'
    spec.dependency 'ShippingReturns'
    spec.dependency 'ECOMUserInterface'
  end