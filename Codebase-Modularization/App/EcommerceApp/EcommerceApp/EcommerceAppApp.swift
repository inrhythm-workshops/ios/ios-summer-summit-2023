//
//  EcommerceAppApp.swift
//  EcommerceApp
//
//  Created by Hamid Mahmood on 7/30/23.
//

import SwiftUI

@main
struct EcommerceAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
