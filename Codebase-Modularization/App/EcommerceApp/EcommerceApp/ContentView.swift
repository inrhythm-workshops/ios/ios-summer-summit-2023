//
//  ContentView.swift
//  EcommerceApp
//
//  Created by Hamid Mahmood on 7/30/23.
//

import SwiftUI
import Authentication
import ProductManagement

struct ContentView: View {
    
    
    var body: some View {
        VStack {
//            LoginView()
            ShoppingCart()
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
