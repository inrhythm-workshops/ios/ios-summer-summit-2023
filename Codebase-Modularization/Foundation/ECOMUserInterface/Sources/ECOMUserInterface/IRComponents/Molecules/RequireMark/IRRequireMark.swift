//
//  IRRequireMark.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

public enum IRLabelRequiredMark: CaseIterable, CustomStringConvertible, Codable {
    case required, optional, none
    public var description: String {
        switch self {
        case .required:
            return "required"
        case .optional:
            return "optional"
        case .none:
            return ""
        }
    }
}
