//
//  IRInputTrailingButton.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public struct IRInputTrailingIconButton {
    public let systemIconName: String
    public let action: () -> Void
    public let accessibilityLabel: String
}
