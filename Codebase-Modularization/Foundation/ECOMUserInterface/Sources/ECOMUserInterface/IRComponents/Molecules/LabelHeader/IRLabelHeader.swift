//
//  IRLabelHeader.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI
struct IRLabelHeader: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.irTooltip) private var tooltipAction: (() -> Void)?
    @Environment(\.irLabelRequireMark) private var labelRequireMark: IRLabelRequiredMark
    let label: String
    let helperText: String?
    
    var body: some View {
        HStack {
            VStack(spacing: 7) {
                HStack(spacing: 1) {
                    IRText(label)
                        .irTextStyle(IRSystemTextStyles.headline)
                        .accessibilityHidden(true)
                    Group {
                        if labelRequireMark == .required {
                            Text(" (")
                            + Text("*")
                                .foregroundColor(isEnabled ? .red : Color("gray.light"))
                            + Text("required)")
                        } else if labelRequireMark == .optional {
                            Text(" (optional)")
                            
                        }
                    }
                    .irFont()
                    .accessibilityHidden(true)
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                if let helperText {
                    IRText(helperText)
                        .irTextStyle(IRSystemTextStyles.contentNote)                        
                        .foregroundColor(isEnabled ? .gray : Color("gray.light"))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .accessibilityHidden(true)
                }
            }
            
            if let tooltipAction {
                Button {
                    tooltipAction()
                } label: {
                    Image(systemName: "questionmark.circle")
                        .padding(12)
                }
                .accessibilityLabel("More information for \(label)")
                .contentShape(RoundedRectangle(cornerRadius: 8))
            }
        }
        .foregroundColor(isEnabled ? .primary : Color("gray.light"))
        .padding(.bottom, tooltipAction != nil && helperText == nil ? 0 : 8)
    }
}
