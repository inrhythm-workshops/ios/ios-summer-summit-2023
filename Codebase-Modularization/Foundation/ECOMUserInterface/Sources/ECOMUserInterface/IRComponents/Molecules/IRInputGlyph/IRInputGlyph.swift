//
//  IRInputGlyph.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public struct IRInputGlyph {
    public let icon: String
    public let accessibilityHint: String
}
