//
//  IRInputTextAlignmentEnvironmentVariables.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public typealias IRInputTextAlignment = (singleLine: Alignment, multiLine: TextAlignment)

private struct IRInputTextAlignmentKey: EnvironmentKey {
    static var defaultValue: IRInputTextAlignment? = nil
}

public extension EnvironmentValues {
    var irInputTextAlignment: IRInputTextAlignment? {
        get { self[IRInputTextAlignmentKey.self] }
        set { self[IRInputTextAlignmentKey.self] = newValue }
    }
}
