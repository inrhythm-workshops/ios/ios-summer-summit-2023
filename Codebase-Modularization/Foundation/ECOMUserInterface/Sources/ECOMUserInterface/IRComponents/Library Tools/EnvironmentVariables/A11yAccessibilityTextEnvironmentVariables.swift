//
//  A11yAccessibilityText.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

struct IRAccessibilityA11YTextKey: EnvironmentKey {
    static var defaultValue: String = ""
}

public extension EnvironmentValues {
    var irAccessibilityA11YText: String {
        get { self[IRAccessibilityA11YTextKey.self] }
        set { self[IRAccessibilityA11YTextKey.self] = newValue }
    }
}
