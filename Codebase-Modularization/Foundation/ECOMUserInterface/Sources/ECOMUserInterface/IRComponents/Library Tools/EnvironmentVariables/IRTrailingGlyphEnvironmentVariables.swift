//
//  IRTrailingGlyphEnvironmentVariables.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

private struct IRInputTrailingGlyphKey: EnvironmentKey {
    static var defaultValue: IRInputGlyph? = nil
}

public extension EnvironmentValues {
    var irInputTrailingGlyph: IRInputGlyph? {
        get { self[IRInputTrailingGlyphKey.self] }
        set { self[IRInputTrailingGlyphKey.self] = newValue }
    }
}
