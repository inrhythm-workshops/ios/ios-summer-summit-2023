//
//  IRInputTrailingIconButton.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

private struct IRInputTrailingIconButtonKey: EnvironmentKey {
    static var defaultValue: IRInputTrailingIconButton? = nil
}

public extension EnvironmentValues {
    var irInputTrailingIconButton: IRInputTrailingIconButton? {
        get { self[IRInputTrailingIconButtonKey.self] }
        set { self[IRInputTrailingIconButtonKey.self] = newValue }
    }
}


