//
//  IRLabelRequireMarkEnvironmentVariables.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

struct IRLabelRequireMarkKey: EnvironmentKey {
    static var defaultValue: IRLabelRequiredMark = .none
}

public extension EnvironmentValues {
    var irLabelRequireMark: IRLabelRequiredMark {
        get { self[IRLabelRequireMarkKey.self] }
        set { self[IRLabelRequireMarkKey.self] = newValue }
    }
}
