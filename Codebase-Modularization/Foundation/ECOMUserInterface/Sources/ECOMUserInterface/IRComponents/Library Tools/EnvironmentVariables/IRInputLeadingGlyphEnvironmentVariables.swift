//
//  IRInputLeadingGlyph.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

private struct IRInputLeadingGlyphKey: EnvironmentKey {
    static var defaultValue: IRInputGlyph? = nil
}
    
public extension EnvironmentValues {
    var irInputLeadingGlyph: IRInputGlyph? {
        get { self[IRInputLeadingGlyphKey.self] }
        set { self [IRInputLeadingGlyphKey.self] = newValue }
    }
}
