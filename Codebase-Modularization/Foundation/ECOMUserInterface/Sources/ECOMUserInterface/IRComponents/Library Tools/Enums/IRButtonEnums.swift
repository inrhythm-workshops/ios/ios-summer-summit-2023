//
//  IRButtonEnums.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

public enum IRButtonSize: CaseIterable, CustomStringConvertible, Codable {
    case large
    case inline
    
    public var description: String {
        switch self {
        case .large:
            return "Large"
        case .inline:
            return "Inline"
        }
    }
}
public enum IRButtonWidth: CaseIterable, CustomStringConvertible, Codable {
    case variable
    case full
    
    public var description: String {
        switch self {
        case .variable:
            return "Variable"
        case .full:
            return "Full"
        }
    }
}
public enum IRButtonType: CaseIterable, CustomStringConvertible, Codable {
    case border
    case filled
    case plain
    case tinted
    
    public var description: String {
        switch self {
        case .filled:
            return "Filled"
        case .tinted:
            return "Tinted"
        case .border:
            return "Border"
        case .plain:
            return "Plain"
        }
    }
}
public enum IRIconButtonShape: CaseIterable, CustomStringConvertible, Codable {
    case square
    case circle
    
    public var description: String {
        switch self {
        case .square:
            return "Square"
        case .circle:
            return "Circle"
        }
    }
}
public enum IRButtonIconAlignment: CaseIterable, CustomStringConvertible, Codable {
    case leading
    case trailing
    
    public var description: String {
        switch self {
        case .leading:
            return "Leading"
        case .trailing:
            return "Trailing"
        }
    }
}
