//
//  IRUtilities.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public class IRUtility {
    static public var shared: IRUtility = IRUtility()
    
    static public func getBundle(_ reference: AnyClass) -> Bundle? {
        let frameworkBundle: Bundle = Bundle(for: reference)
        let module: String = String(String(reflecting: self).prefix { $0 != "."})
        let filePath: String = (frameworkBundle.resourcePath ?? "") + "/\(module).bundle"
        let retval: Bundle? = Bundle(path: filePath)
        if retval == nil {
            return frameworkBundle
        } else {
            return retval
        }
    }
}

public func getFontFamily() -> String {
    UIFont.registerFontsIfNeeded()
    return "ProximaNova-Regular"
    //return "DroidSerif"
}
