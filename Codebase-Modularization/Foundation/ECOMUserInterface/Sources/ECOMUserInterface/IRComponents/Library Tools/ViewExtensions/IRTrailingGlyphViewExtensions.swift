//
//  IRTrailingGlyphViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public extension View {
    func irInputTrailingGlyph(_ character: String, accessibilityHint: String) -> some View {
        environment(\.irInputTrailingGlyph, IRInputGlyph(icon: character, accessibilityHint: accessibilityHint))
    }
}
