//
//  IRLabelRequireMarkViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public extension View {
    func irLabelRequireMark(_ mark: IRLabelRequiredMark) -> some View {
        environment(\.irLabelRequireMark, mark)
    }
}
