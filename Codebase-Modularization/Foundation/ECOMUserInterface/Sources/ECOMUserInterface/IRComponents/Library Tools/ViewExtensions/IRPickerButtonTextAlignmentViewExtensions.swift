//
//  IRPickerButtonTextAlignmentViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public extension View {
    func irPickerButtonTextAlignment(_ alignment: Alignment = .leading) -> some View {
        environment(\.irPickerButtonTextAlignment, alignment)
    }
}

