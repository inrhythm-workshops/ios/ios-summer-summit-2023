//
//  A11yAccessibilityText.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

public extension View {
    func irAccessibilityA11yText(_ text: String) -> some View {
        environment(\.irAccessibilityA11YText, text)
    }
}
