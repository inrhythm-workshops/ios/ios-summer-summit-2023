//
//  IRInputTrailingIconButtonViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public extension View {
    func irInputTrailingIconButton(_ systemIconName: String, accessibilityLabel: String, action: @escaping () -> Void) -> some View {
        environment(\.irInputTrailingIconButton, IRInputTrailingIconButton(systemIconName: systemIconName, action: action, accessibilityLabel: accessibilityLabel))
    }
}
