//
//  IRIconButton.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI
public struct IRIconButton: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.irButtonColorConfig) private var style: IRButtonColor
    
    public let action: () -> Void
    private var iconName: String
    @State private var shape: IRIconButtonShape
    private var label: String
    
    public init(iconName: String, action: @escaping () -> Void, shape: IRIconButtonShape, label: String = "Button") {
        self.action = action
        self.iconName = iconName
        self.shape = shape
        self.label = label
    }
    
    public var body: some View {
        Button { action() }
        label: {
            Image(systemName: iconName)
                .irFont()
                .accessibilityLabel(label)
                .padding(.init(top: 10, leading: 10, bottom: 10, trailing: 10))
        }
        .frame(alignment: .center)
        .buttonStyle(IRIconButtonStyle(shape: shape))
    }
}


struct IRIconButton_filled_Previews: PreviewProvider {
    static var previews: some View {
        IRIconButton(iconName: "xmark.app", action: {}, shape: .square)
            .IRButtonType(.filled)
    }
}

struct IRIconButton_border_Previews: PreviewProvider {
    static var previews: some View {
        IRIconButton(iconName: "xmark.app", action: {}, shape: .circle)
            .IRButtonType(.border)
    }
}
