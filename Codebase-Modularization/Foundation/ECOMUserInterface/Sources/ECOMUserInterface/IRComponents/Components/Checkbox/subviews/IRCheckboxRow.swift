//
//  IRCheckboxRow.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI
public struct IRCheckboxRow: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.dynamicTypeSize) private var dynamicTypeSize: DynamicTypeSize
    @Environment(\.irAccessibilityA11YText) private var allyText: String
    @Environment(\.irCheckboxIndeterminateEnabled) private var indeterminateEnabled: Bool
    
    private let label: String
    private let value: String?
    private let sublabel: String?
    private let subvalue: String?
    private let tag: String
    
    @Binding var selection: [String]
    @Binding var allTags: [String]
    @Binding var isOn: Bool
    
    public init(label: String, value: String? = nil, sublabel: String? = nil, subvalue: String? = nil, selection: Binding<[String]>, tag: String, allTags: Binding<[String]>, isOn: Binding<Bool>) {
        self.label = label
        self.value = value
        self.sublabel = sublabel
        self.subvalue = subvalue
        self._selection = selection
        self.tag = tag
        self._allTags = allTags
        self._isOn = isOn
    }
    
    public var body: some View {
        VStack {
            if dynamicTypeSize > .accessibility2 {
                HStack(alignment: .top) {
                    IRCheckboxRowLabel(label: label, value: value, sublabel: sublabel, subvalue: subvalue, selection: $selection, tag: tag, allTags: $allTags, isOn: $isOn)
                        .fixedSize(horizontal: false, vertical: true)
                }
                HStack(alignment: .top) {
                    IRCheckboxRowValue(value: value, sublabel: sublabel, subvalue: subvalue, selection: $selection)
                        .fixedSize(horizontal: false, vertical: true)
                }
            } else {
                HStack(alignment: .top, spacing: 0) {
                    IRCheckboxRowLabel(label: label, value: value, sublabel: sublabel, subvalue: subvalue, selection: $selection, tag: tag, allTags: $allTags, isOn: $isOn)
                        .fixedSize(horizontal: false, vertical: true)
                }
                
                IRCheckboxRowValue(value: value, sublabel: sublabel, subvalue: subvalue, selection: $selection)
                    .fixedSize(horizontal: false, vertical: true)
                    .frame(maxWidth: .infinity)
            }
        }
        .onAppear(perform: {
            if selection.contains(tag) {
                isOn = true
            } else {
                isOn = false
            }
        })
        .frame(maxWidth: .infinity, alignment: .top)
        .accessibilityElement(children: .ignore)
        .accessibilityAddTraits(selection.contains(tag) ? .isSelected : .isStaticText)
    }
}

struct IRCheckboxRowChecked_Previews: PreviewProvider {
    @State static var selection: [String] = [String]()
    @State static var allTags: [String] = [String]()
    static var previews: some View {
        IRCheckboxRow(label: "Label", selection: $selection, tag: "a", allTags: $allTags, isOn: .constant(true))
    }
}

struct IRCheckboxRowNotChecked_Previews: PreviewProvider {
    @State static var selection: [String] = [String]()
    @State static var allTags: [String] = [String]()
    static var previews: some View {
        IRCheckboxRow(label: "Label", selection: $selection, tag: "a", allTags: $allTags, isOn: .constant(false))
    }
}
