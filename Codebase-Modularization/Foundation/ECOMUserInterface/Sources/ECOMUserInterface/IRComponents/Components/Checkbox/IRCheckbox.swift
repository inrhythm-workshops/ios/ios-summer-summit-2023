//
//  IRCheckbox.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI
public struct IRCheckbox: View {
    
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.dynamicTypeSize) private var dynamicTypeSize: DynamicTypeSize
    @Environment(\.irAccessibilityA11YText) private var a11yText: String
    
    private let label: String
    private let value: String?
    private let sublabel: String?
    private let subvalue: String?
    private let tag: String
    @State var isOn: Bool = false
    @Binding var selection: [String]
    @Binding var allTags: [String]
    
    public init(label: String, value: String? = nil, sublabel: String? = nil, subvalue: String? = nil, selection: Binding<[String]>, tag: String, allTags: Binding<[String]>) {
        self.label = label
        self.value = value
        self.sublabel = sublabel
        self.subvalue = subvalue
        self._selection = selection
        self.tag = tag
        self._allTags = allTags
    }
    
    public var body: some View {
        Toggle(isOn: $isOn, label: {})
            .toggleStyle(IRCheckboxToggleStyle(label: label, value: value, sublabel: sublabel, subvalue: subvalue, tag: tag, selection: $selection, allTags: $allTags, isEnabled: isEnabled))
            .accessibilityRepresentation {
                Toggle(isOn: $isOn, label: {
                    EmptyView()
                        .accessibilityLabel([label, sublabel ?? "", value ?? "", subvalue ?? "", a11yText]
                            .filter { $0.count > 0 }.joined(separator: ", "))
                })
            }
    }
}

struct IRCheckBoxRow_Previews: PreviewProvider {
    @State static var selection: [String] = [String]()
    @State static var allTags: [String] = [String]()
    
    static var previews: some View {
        ScrollView {
            IRCheckbox(label: "Checkbox", selection: $selection, tag: "A", allTags: $allTags)
            IRCheckbox(label: "Checkbox", selection: $selection, tag: "B", allTags: $allTags)
            IRCheckbox(label: "Checkbox", selection: $selection, tag: "C", allTags: $allTags)
        }
    }
}

struct IRCheckBoxRow_Indeterminate_Previews: PreviewProvider {
    @State static var selection: [String] = [String]()
    @State static var allTags: [String] = [String]()
    
    static var previews: some View {
        ScrollView {
            IRCheckbox(label: "Checkbox", selection: $selection, tag: "A", allTags: $allTags)
            IRCheckbox(label: "Checkbox", selection: $selection, tag: "B", allTags: $allTags)
            IRCheckbox(label: "Checkbox", selection: $selection, tag: "C", allTags: $allTags)
        }
        .irCheckboxIndeterminateEnabled(true)
    }
}
