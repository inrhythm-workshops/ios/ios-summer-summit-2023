//
//  ButtonColor.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

public struct IRButtonColor: Hashable, CustomStringConvertible {
    public var description: String {
        return type.description
    }
    
    var type: IRButtonType
    // these should not be "exposed" to the consumer... only set by the system
    var backgroundColor: Color
    var borderColor: Color
    var foregroundColor: Color
    var hoverBackgroundColor: Color
    var hoverBorderColor: Color
    var hoverForegroundColor: Color
    var activeBackgroundColor: Color
    var activeBorderColor: Color
    var activeForegroundColor: Color
    var disabledBackgroundColor: Color
    var disabledBorderColor: Color
    var disabledForegroundColor: Color
    var spacing: CGFloat
    var strokeWidth: CGFloat
    
    public init(type: IRButtonType = .filled) {
        self.type = type
        switch type {
        case .border:
            self.backgroundColor = Color("Secondary")
            self.borderColor = Color("IRBrand")
            self.foregroundColor = Color("IRBrand")
            self.hoverBackgroundColor = Color("IRBrand").opacity(0.90)
            self.hoverBorderColor = Color("IRBrand").opacity(0.90)
            self.hoverForegroundColor = .white
            self.activeBackgroundColor = Color("IRBrand").opacity(0.50)
            self.activeBorderColor = Color("IRBrand").opacity(0.50)
            self.activeForegroundColor = Color("Primary")
            self.disabledBackgroundColor = Color("Gray.UltraLight")
            self.disabledBorderColor = Color("Gray.Light")
            self.disabledForegroundColor = Color("Gray.Light")
            self.spacing = 5
            self.strokeWidth = 1
        case .filled:
            self.backgroundColor = Color("IRBrand")
            self.borderColor = Color("IRBrand")
            self.foregroundColor = Color("Secondary")
            self.hoverBackgroundColor = Color("Secondary")
            self.hoverBorderColor = Color("IRBrand")
            self.hoverForegroundColor = Color("IRBrand")
            self.activeBackgroundColor = Color("Secondary")
            self.activeBorderColor = Color("Secondary")
            self.activeForegroundColor = Color("Secondary")
            self.disabledBackgroundColor = Color("Gray.UltraLight")
            self.disabledBorderColor = Color("Gray.Light")
            self.disabledForegroundColor = Color("Gray.Light")
            self.spacing = 5
            self.strokeWidth = 1
        case .plain:
            self.backgroundColor = .clear
            self.borderColor = .clear
            self.foregroundColor = Color("IRBrand")
            self.hoverBackgroundColor = Color("IRBrandSubtle")
            self.hoverBorderColor = Color("IRBrandSubtle")
            self.hoverForegroundColor = Color("Secondary")
            self.activeBackgroundColor = Color("Secondary")
            self.activeBorderColor = Color("Secondary")
            self.activeForegroundColor = Color("Secondary")
            self.disabledBackgroundColor = Color("Gray.UltraLight")
            self.disabledBorderColor = Color("Gray.Light")
            self.disabledForegroundColor = Color("Gray.Light")
            self.spacing = 5
            self.strokeWidth = 1
        case .tinted:
            self.backgroundColor = .white
            self.borderColor = .white
            self.foregroundColor = .white
            self.hoverBackgroundColor = .white
            self.hoverBorderColor = .white
            self.hoverForegroundColor = .white
            self.activeBackgroundColor = .white
            self.activeBorderColor = .white
            self.activeForegroundColor = .white
            self.disabledBackgroundColor = Color("Gray.UltraLight")
            self.disabledBorderColor = Color("Gray.Light")
            self.disabledForegroundColor = Color("Gray.Light")
            self.spacing = 5
            self.strokeWidth = 1
        }
    }
}
