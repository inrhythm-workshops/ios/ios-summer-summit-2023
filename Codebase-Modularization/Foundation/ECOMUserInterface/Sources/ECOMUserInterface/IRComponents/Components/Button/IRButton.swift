//
//  IRButton.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

public struct IRButton: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.irButtonSizeConfig) private var buttonSize: IRButtonSize
    @Environment(\.irButtonWidthConfig) private var buttonwidth: IRButtonWidth
    @Environment(\.irButtonIconAlignment) private var buttonIconAlignment: IRButtonIconAlignment
    
    @State private var isOnHover: Bool = false
    
    private let iconString: String?
    public let action: () -> Void
    public let label: Text
    public init(label: String, action: @escaping () -> Void, iconString: String? = nil) {
        self.action = action
        self.label = Text(label)
        self.iconString = iconString
    }
    
    public var body: some View {
        Button {
            action()
        } label: {
            HStack {
                if let iconString = iconString {
                    if buttonIconAlignment == .leading {
                        Image(systemName: iconString)
                            .padding(.leading, 16)
                            .accessibilityHidden(true)
                        self.label
                        if buttonIconAlignment == .trailing {
                            Image(systemName: iconString)
                                .padding(.trailing, 16)
                                .accessibilityHidden(true)
                        }
                    }
                } else {
                    self.label
                }
            }
            .if(buttonwidth == .full) { view in
                view.frame(maxWidth: .infinity)
            }
            .if(buttonSize == .large) { view in
                view.padding(.init(top: 12, leading: buttonIconAlignment == .leading && iconString != nil ? 0 : 16, bottom: 12, trailing: buttonIconAlignment == .trailing && iconString != nil ? 0 : 16))
            }
            .if(buttonSize == .inline) { view in
                view.padding(.init(top: 8, leading: buttonIconAlignment == .leading && iconString != nil ? 0 : 16, bottom: 8, trailing: buttonIconAlignment == .trailing && iconString != nil ? 0: 16))
            }
            .if(buttonwidth == .full) { view in
                view.frame(minWidth: 10, idealWidth: .infinity, maxWidth: .infinity)
                    .padding(0)
                    .frame(alignment: .center)
                    .cornerRadius(8)
            }
        }
        .cornerRadius(8)
        .buttonStyle(IRButtonStyle())
    }
}

