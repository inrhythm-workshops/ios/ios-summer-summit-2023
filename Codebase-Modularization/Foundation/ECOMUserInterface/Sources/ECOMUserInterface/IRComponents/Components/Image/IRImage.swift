//
//  IRImage.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//
import SwiftUI

public struct IRImage: View {
    
    @Environment(\.irAccessibilityA11YText)
    private var a11yText: String
    var source: Image?
    var url: URL?
    var caption: String
    var ratio: IRImageRatio
    var showCaption: Bool = false
    var cornerRadius: CGFloat
    @State var imageFromURL: Image?
    @State private var measurement: CGSize = CGSize.zero
    
    public init(source: Image? = nil, caption: String, ratio: IRImageRatio = .sixteenNine, showCaption: Bool = false, cornerRadius: CGFloat = 0) {
        self.source = source
        self.caption = caption
        self.ratio = ratio
        self.showCaption = showCaption
        self.cornerRadius = cornerRadius
    }
    
    public var body: some View {
        if source != nil || url != nil {
            switch ratio {
            case .original:
                if let url = url {
                    VStack(alignment: .leading) {
                        AsyncImage(url: url, content: { phase in
                            phase
                                .resizable()
                                .scaledToFit()
                                .readSize { size in
                                    measurement = size
                                }
                                .clipped()
                                .cornerRadius(cornerRadius)
                                .if(a11yText != "") { view in
                                    view.accessibilityLabel(a11yText)
                                }
                                .if(a11yText == "") { view in
                                    view.accessibilityHidden(true)
                                }
                            
                            if showCaption {
                                Text(caption)
                                    .irFont()
                                    .foregroundColor(.primary)
                                    .ignoresSafeArea(.all, edges: .all)
                                    .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                                    .padding(.top, 0)
                            }
                        }, placeholder: {
                            IRFillShimmer()
                                .readSize { size in
                                    measurement = size
                                }
                                .if(a11yText != "") { view in
                                    view.accessibilityLabel(a11yText)
                                        .if(a11yText == "" ) { view in
                                            view.accessibilityHidden(true)
                                        }
                                }
                            if showCaption {
                                Text(caption)
                                    .irFont()
                                    .foregroundColor(.primary)
                                    .ignoresSafeArea(.all, edges: .all)
                                    .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                                    .padding(.top, 0)
                            }
                        })
                    }
                } else {
                    VStack(alignment:.leading) {
                        source?
                            .resizable()
                            .scaledToFit()
                            .readSize { size in
                                measurement = size
                            }
                            .clipped()
                            .cornerRadius(cornerRadius)
                            .if(a11yText != "") { view in
                                view.accessibilityLabel(a11yText)
                            }
                            .if(a11yText == "") { view in
                                view.accessibilityHidden(true)
                            }
                        if showCaption {
                            Text(caption)
                                .irFont()
                                .foregroundColor(.primary)
                                .ignoresSafeArea(.all, edges: .all)
                                .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                                .padding(.top, 0)
                        }
                    }
                }
            default:
                if let url = url {
                    VStack(alignment: .leading) {
                        AsyncImage(url: url, content: { phase in
                            Rectangle()
                                .readSize{ size in
                                    measurement = size
                                }
                                .aspectRatio(ratio.value, contentMode: .fit)
                                .overlay(
                                    phase
                                        .resizable()
                                        .scaledToFill()
                                        .if(a11yText != "") { view in
                                            view.accessibilityLabel(a11yText)
                                                .if(a11yText == "") { view in
                                                    view.accessibilityHidden(true)
                                                }
                                        }
                                        .clipShape(Rectangle())
                                        .cornerRadius(cornerRadius))
                            
                            if showCaption {
                                Text(caption)
                                    .irFont()
                                    .foregroundColor(.primary)
                                    .ignoresSafeArea(.all, edges: .all)
                                    .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                            }
                        }, placeholder: {
                            IRFillShimmer()
                                .aspectRatio(ratio.value, contentMode: .fit)
                                .overlay(
                                    IRFillShimmer()
                                        .if(a11yText != "") { view in
                                            view.accessibilityLabel(a11yText)
                                        }
                                        .if(a11yText == "") { view in
                                            view.accessibilityHidden(true)
                                        }
                                        .clipShape(Rectangle())
                                        .cornerRadius(cornerRadius)
                                )
                            if showCaption {
                                Text(caption)
                                    .irFont()
                                    .foregroundColor(.primary)
                                    .ignoresSafeArea(.all, edges: .all)
                                    .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                            }
                        })
                    }
                } else {
                    VStack(alignment: .leading) {
                        Rectangle()
                            .readSize { size in
                                measurement = size
                            }
                            .aspectRatio(ratio.value, contentMode: .fit)
                            .overlay(
                                source?
                                    .resizable()
                                    .scaledToFill()
                                    .if(a11yText != "") { view in
                                        view.accessibilityLabel(a11yText)
                                    }
                                    .if(a11yText == "") { view in
                                        view.accessibilityHidden(true)
                                    }
                                    .clipShape(Rectangle())
                                    .cornerRadius(cornerRadius)
                            )
                        if showCaption {
                            Text(caption)
                                .irFont()
                                .foregroundColor(.primary)
                                .ignoresSafeArea(.all, edges: .all)
                                .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                        }
                    }
                }
            }
        } else {
            VStack {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
                    .readSize { size in
                        measurement = size
                    }
                    .clipped()
                    .if(a11yText != "") { view in
                        view.accessibilityLabel(a11yText)
                    }
                    .if(a11yText == "") { view in
                        view.accessibilityHidden(true)
                    }
                    .foregroundColor(.primary)
                
                if showCaption {
                    Text(caption)
                        .irFont()
                        .foregroundColor(.primary)
                        .ignoresSafeArea(.all, edges: .all)
                        .frame(minWidth: measurement.width, maxWidth: .infinity, alignment: .leading)
                }
            }
        }
    }
}

extension IRImage {
    public init(url: URL, caption: String, ratio: IRImageRatio = .sixteenNine, showCaption: Bool = false, cornerRadius: CGFloat = 0) {
        self.source = nil
        self.url = url
        self.caption = caption
        self.ratio = ratio
        self.showCaption = showCaption
        self.cornerRadius = cornerRadius
    }
}

extension IRImage {
    public init(url: String, caption: String, ratio: IRImageRatio = .sixteenNine, showCaption: Bool = false, cornerRadius: CGFloat = 0) {
        self.source = nil
        if let url = URL(string: url) {
            self.url = url
        } else {
            self.url = nil
        }
        
        self.caption = caption
        self.ratio = ratio
        self.showCaption = showCaption
        self.cornerRadius = cornerRadius
    }
}

