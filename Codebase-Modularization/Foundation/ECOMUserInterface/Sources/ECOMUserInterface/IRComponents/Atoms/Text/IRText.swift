//
//  IRText.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

public struct IRTextStyle {
    var size: CGFloat
    var modifiers: [IRTextModifier]
    var relativeTo: Font.TextStyle
    var color: Color
    
    public init(size: CGFloat, modifiers: [IRTextModifier] = [IRTextModifier](), relativeTo: Font.TextStyle = .body, color: Color) {
        self.size = size
        self.modifiers = modifiers
        self.relativeTo = relativeTo
        self.color = color
    }
}

public class IRSystemTextStyles {
    public init() {}
    public static let content = IRTextStyle(size: 14, color: Color("Primary"))
    public static let contentNote = IRTextStyle(size: 16, color: Color("Gray.Light"))
    public static let body = IRTextStyle(size: 17, relativeTo: .body, color: Color("Primary"))
    static let callout = IRTextStyle(size: 16, relativeTo: .callout, color: Color("Primary"))
    static let caption = IRTextStyle(size: 14, modifiers: [.Italic], relativeTo: .caption, color: Color("Primary"))
    static let caption2 = IRTextStyle(size: 16, modifiers: [.Italic], relativeTo: .caption2, color: Color("Primary"))
    static let footnote = IRTextStyle(size: 14, modifiers: [.Italic], relativeTo: .footnote, color: Color("Gray.Light"))
    static let headline = IRTextStyle(size: 17, modifiers: [.Bold], relativeTo: .headline, color: Color("Primary"))
    static let title = IRTextStyle(size: 28, relativeTo: .title, color: Color("Primary"))
    static let title2 = IRTextStyle(size: 22, relativeTo: .title2, color: Color("Primary"))
    static let title3 = IRTextStyle(size: 20, relativeTo: .title3, color: Color("Primary"))
    public static let largeTitle = IRTextStyle(size: 34, modifiers: [.Bold], relativeTo: .largeTitle, color: Color("Primary"))
}


public struct IRText: View {
    @Environment(\.irTextColor) var color: Color?
    @Environment(\.irTextStyle) var style: IRTextStyle
    @Environment(\.isEnabled) var enabled: Bool
    
    var text: String
    
    public init(_ text: String) {
        self.text = text
    }
    
    public var body: some View {
        if #available(iOS 16.0, *) {
            Text(text)
                .foregroundColor(!enabled ? Color("Gray.UltraLight") : color != nil ? color : style.color)
                .italic(style.modifiers.contains(.Italic) ? true : false)
                .monospaced(style.modifiers.contains(.Monospaced) ? true : false)
                .underline(style.modifiers.contains(.Underline) ? true : false)
                .bold(style.modifiers.contains(.Bold) ? true : false)
                .font(.custom("ProximaNova-Regular", size: style.size, relativeTo: style.relativeTo))
        } else {
            // Fallback on earlier versions
        }
    }
}
