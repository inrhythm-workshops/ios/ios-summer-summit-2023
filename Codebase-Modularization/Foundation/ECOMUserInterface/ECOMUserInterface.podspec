Pod::Spec.new do |spec|
    spec.name          = 'ECOMUserInterface'
    spec.version       = '1.0.0'
    spec.summary       = 'ECOMUserInterface Library'
    spec.license       = { :type => 'MIT', :file => 'License.md'}
    spec.homepage      = 'https://gitlab.com/'
    spec.source        = { :git => ' ', :tag => spec.version, :path => '/' }
    spec.author = {'Mahmood, Hamid' => 'hmahmood@inrhythm.com'}
    spec.module_name   = 'ECOMUserInterface'
    spec.ios.deployment_target  = '15.0'
    spec.watchos.deployment_target  = '6'
    spec.swift_version = '5'
    spec.source_files       = 'Sources/ECOMUserInterface/**/*.swift'
    spec.resources = [
      'Sources/ECOMUserInterface/*.strings',
      'Sources/ECOMUserInterface/*.{xcassets}',
    ]
  end