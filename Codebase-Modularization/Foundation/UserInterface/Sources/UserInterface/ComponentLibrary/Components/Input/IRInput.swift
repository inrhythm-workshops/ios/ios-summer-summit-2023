//
//  IRInput.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI
public struct IRInput: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.irInputTextAlignment) private var alignment: IRInputTextAlignment?
    @Environment(\.irLabelRequireMark) private var labelRequireMark: IRLabelRequiredMark
    @Environment(\.irTooltip) private var tooltipAction: (() -> Void)?
    @Environment(\.irInputTrailingIconButton) private var trailingIconButton: IRInputTrailingIconButton?
    @Environment(\.irInputLeadingGlyph) private var leadingGlyph: IRInputGlyph?
    @Environment(\.irInputTrailingGlyph) private var trailingGlyph: IRInputGlyph?
    
    @State private var isOnHover: Bool = false
    @State private var textEditorHeight: CGFloat = 48
    @FocusState private var isFocused: Bool
    
    let label: String
    let helperText: String?
    @Binding var text: String
    let placeholder: String
    let onCommit: () -> Void
    @Binding var isError: Bool
    let errorMessage: String
    
    public init(label: String, helperText: String? = nil, text: Binding<String>, placeholder: String, validationAction: @escaping () -> Void = {}, isError: Binding<Bool> = .constant(false), errorMessage: String = "") {
        self.label = label
        self.helperText = helperText
        self._text = text
        self.placeholder = placeholder
        self.onCommit = validationAction
        self._isError = isError
        self.errorMessage = errorMessage
    }
    
    public var body: some View {
        VStack(spacing: 0) {
            IRLabelHeader(label: label, helperText: helperText)
            
            ZStack {
                textInput
                    .padding(.leading, leadingGlyph == nil ? 12 : 28)
                    .padding(.trailing, trailingGlyph == nil && trailingIconButton == nil ? 12 : 0)
                    .padding(.trailing, trailingGlyph != nil && trailingIconButton != nil ? 66 : 0)
                    .padding(.trailing, trailingGlyph != nil && trailingIconButton == nil ? 34 : 0)
                    .padding(.trailing, trailingGlyph == nil && trailingIconButton != nil ? 38 : 0)
                    .irFont()
                    .foregroundColor(isEnabled ? .black : Color("gray.light"))
                    .frame(minHeight: 48)
                    .background(.white)
                    .cornerRadius(8)
                    .overlay(IRBorder(isFocused: $isFocused, isError: $isError))
                    .contentShape(RoundedRectangle(cornerRadius: 8))
                    .accessibilitySortPriority(2)
                
                HStack(spacing: 0) {
                    if leadingGlyph != nil {
                        Text(leadingGlyph!.icon)
                            .padding(.leading, 14)
                            .foregroundColor(isEnabled ? .primary : Color("gray.light"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .accessibilityHidden(true)
                    }
                    
                    if trailingGlyph != nil {
                        Text(trailingGlyph!.icon)
                            .padding(.trailing, trailingIconButton != nil ? 0: 14)
                            .foregroundColor(isEnabled ? .primary : Color("gray.light"))
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .accessibilityHidden(true)
                    }
                    
                    if trailingIconButton != nil {
                        Button {
                            trailingIconButton!.action()
                        } label: {
                            Image(systemName: trailingIconButton!.systemIconName)
                                .foregroundColor(isEnabled ? .primary : Color("gray.light"))
                                .padding(14)
                        }
                        .contentShape(RoundedRectangle(cornerRadius: 8))
                        .frame(maxWidth: leadingGlyph == nil && trailingGlyph == nil ? .infinity : nil, alignment: .trailing)
                        .accessibilityLabel(trailingIconButton!.accessibilityLabel)
                        .accessibilitySortPriority(1)
                    }
                }
            }
            
            if isError && !errorMessage.isEmpty {
                IRErrorLabel(errorMessage: errorMessage)
            }
        }
        .accessibilityElement(children: .contain)
        .frame(maxWidth: .infinity)
        .onTapGesture {
            isFocused = false
        }
    }
    
    @ViewBuilder
    private var textInput: some View {
        TextField(text: $text) {
            IRText(placeholder)
                .irTextStyle(IRSystemTextStyles.contentNote)
        }
        .accessibilityLabel("\(label), \(labelRequireMark.description), \(helperText ?? ""), \(isError ? "Error, \(errorMessage)": "")")
        .accessibilityHint(leadingGlyph?.accessibilityHint ?? trailingGlyph?.accessibilityHint ?? "")
        .autocorrectionDisabled()
        .padding(.vertical, 8)
        .foregroundColor(isEnabled ? .primary: Color("gray.light"))
        .frame(maxWidth: .infinity, alignment: alignment?.singleLine ?? .leading)
        .multilineTextAlignment(alignment?.multiLine ?? .leading)
        .focused($isFocused)
        .onSubmit {
            isFocused = false
            isError = false
            onCommit()
        }
        .onChange(of: isFocused) { isFocused in
            if isFocused == false {
                isError = false
                onCommit()
            }
        }
        .onHover { isHover in
            isOnHover = isHover
        }
    }
    
    struct ViewHeightKey: PreferenceKey {
        static var defaultValue: CGFloat { 0 }
        static func reduce(value: inout Value, nextValue: () -> Value) {
            value = value + nextValue()
        }
    }
}

struct IRInput_Previews: PreviewProvider {
    
    @State static var name: String = ""
    static var previews: some View {
        IRInput(label: "Name", helperText: "Helper", text: $name, placeholder: "Name", validationAction: {}, isError: .constant(false), errorMessage: "")
    }
}
