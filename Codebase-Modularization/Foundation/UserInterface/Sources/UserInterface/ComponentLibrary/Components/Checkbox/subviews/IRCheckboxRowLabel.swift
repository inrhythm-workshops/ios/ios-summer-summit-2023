//
//  IRCheckboxRowLabel.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

struct IRCheckboxRowLabel: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.dynamicTypeSize) private var dynamicTypeSize: DynamicTypeSize
    @Environment(\.irCheckboxIndeterminateEnabled) private var indeterminateEnabled: Bool
    
    private let label: String
    private let value: String?
    private let sublabel: String?
    private let subvalue: String?
    private let tag: String
    
    @Binding var selection: [String]
    @Binding private var allTags: [String]
    @Binding var isOn: Bool
    
    public init(label: String, value: String? = nil, sublabel: String? = nil, subvalue: String? = nil, selection: Binding<[String]>, tag: String, allTags: Binding<[String]>, isOn: Binding<Bool>) {
        self.label = label
        self.value = value
        self.sublabel = sublabel
        self.subvalue = subvalue
        self._selection = selection
        self.tag = tag
        self._allTags = allTags
        self._isOn = isOn
    }
    
    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            if indeterminateEnabled {
                Image(systemName: !isOn ? "square": Array(Set(selection)).count == Array(Set(allTags)).count ? "checkmark.square.fill" : "minus.square.fill")
                    .foregroundColor(isEnabled ? Color("IRBrand") :  Color("Gray.light"))
                    .irFont()
                    .padding(.trailing, 8)
                    .frame(alignment: .centerLastTextBaseline)
                Spacer()
            } else {
                Image(systemName: isOn ? "checkmark.square.fill" : "square")
                    .foregroundColor(isEnabled ? Color("IRBrand") : Color("Gray.UltraLight"))
                    .irFont()
                    .padding(.trailing, 8)
                    .frame(alignment: .centerLastTextBaseline)
                Spacer()
            }
        }
        .frame(alignment: .topLeading)
        .accessibilityHidden(true)
        
        VStack(spacing: 0) {
            Text(label)
                .padding(.bottom, 2)
                .irFont()
                .foregroundColor(isEnabled ? Color("Primary") : Color("Gray.Light"))
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            if let subLabel = sublabel {
                Text(subLabel)
                    .irFont()
                    .foregroundColor(isEnabled ? Color("Gray.Standard") : Color("Gray.Light"))
                    .multilineTextAlignment(.leading)
                    .frame(maxWidth: .infinity, alignment:.topLeading)
            }
        }
        .padding(.trailing, 8)
        .frame(alignment: .top)
    }
}

struct IRCheckboxRowLabel_Previews: PreviewProvider {
    @State static var selection: [String] = [String]()
    @State static var allTags: [String] = [String]()
    static var previews: some View {
        IRCheckboxRowLabel(label: "Label", selection: $selection, tag: "a", allTags: $allTags, isOn: .constant(true))
    }
}
