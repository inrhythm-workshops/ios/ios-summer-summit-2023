//
//  IRCheckboxRowValue.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

struct IRCheckboxRowValue: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.dynamicTypeSize) private var dynamicTypeSize: DynamicTypeSize
    
    private let value: String?
    private let sublabel: String?
    private let subvalue: String?
    
    @Binding var selection: [String]
    
    public init(value: String? = nil, sublabel: String? = nil, subvalue: String? = nil, selection: Binding<[String]>) {

        self.value = value
        self.sublabel = sublabel
        self.subvalue = subvalue
        self._selection = selection
    }
    
    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            Image(systemName: "square")
                .foregroundColor(.clear)
                .irFont()
                .padding(.trailing, 8)
                .frame(alignment: .centerLastTextBaseline)
            Spacer()
        }
        
        VStack(alignment: dynamicTypeSize > .accessibility2 ? .leading : .trailing, spacing: 0) {
            if let value = value {
                Text(value)
                    .irFont()
                    .foregroundColor(isEnabled ? Color("Primary") : Color("Gray.Standard"))
                    .frame(maxWidth: .infinity, alignment: dynamicTypeSize > .accessibility2 ? .topLeading : .topTrailing)
                    .multilineTextAlignment(dynamicTypeSize > .accessibility2 ? .leading : .trailing)
            }
            
            if let subvalue = subvalue {
                Text(subvalue)
                    .irFont()
                    .foregroundColor(isEnabled ? Color("Gray.standard") : Color("Gray.Light"))
                    .frame(maxWidth: .infinity, alignment: dynamicTypeSize > .accessibility2 ? .topLeading : .topTrailing)
                    .multilineTextAlignment(dynamicTypeSize > .accessibility2 ? .leading : .trailing)
            }
        }
        .frame(alignment: dynamicTypeSize > .accessibility2 ? .topLeading : .topTrailing)
    }
}


struct IRCheckboxRowValue_Previews: PreviewProvider {
    @State static var selection: [String] = [String]()
    
    static var previews: some View {
        HStack {
            IRCheckboxRowValue(value: "Value", selection: $selection)
        }
    }
}
