//
//  IRToolTipEnvironmentVariables.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

private struct IRTooltipKey: EnvironmentKey {
    static var defaultValue: (() -> Void)? = nil
}

public extension EnvironmentValues {
    var irTooltip: (() -> Void)? {
        get { self[IRTooltipKey.self] }
        set { self[IRTooltipKey.self] = newValue }
    }
}
