//
//  IRButton.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

struct IRButtonAlignment: EnvironmentKey {
    static var defaultValue: Alignment? = nil
}
struct IRButtonWidthConfig: EnvironmentKey {
    static var defaultValue: IRButtonWidth = .variable
}
struct IRButtonColorConfig: EnvironmentKey {
    static var defaultValue: IRButtonColor = .init()
}
struct IRButtonIconAlignmentConfig: EnvironmentKey {
    static var defaultValue: IRButtonIconAlignment = .leading
}
struct IRButtonSizeConfig: EnvironmentKey {
    static var defaultValue: IRButtonSize = .inline
}


extension EnvironmentValues {
    
    var irButtonSizeConfig: IRButtonSize {
        get { self[IRButtonSizeConfig.self] }
        set { self[IRButtonSizeConfig.self] = newValue }
    }
    
    var irButtonAlignment: Alignment? {
        get { self[IRButtonAlignment.self] }
        set { self[IRButtonAlignment.self] = newValue }
    }
    
    var irButtonWidthConfig: IRButtonWidth {
        get { self[IRButtonWidthConfig.self] }
        set { self[IRButtonWidthConfig.self] = newValue }
    }
    
    var irButtonColorConfig: IRButtonColor {
        get { self[IRButtonColorConfig.self] }
        set { self[IRButtonColorConfig.self] = newValue }
    }
    
    var irButtonIconAlignment: IRButtonIconAlignment {
        get { self[IRButtonIconAlignmentConfig.self] }
        set { self[IRButtonIconAlignmentConfig.self] = newValue }
        
    }
}








