//
//  IRPickerButtonTextAlignmentEnvironmentVariables.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

private struct IRPickerButtonTextAlignmentKey: EnvironmentKey {
    static var defaultValue: Alignment = .leading
}

public extension EnvironmentValues {
    var irPickerButtonTextAlignment: Alignment {
        get { self[IRPickerButtonTextAlignmentKey.self] }
        set { self[IRPickerButtonTextAlignmentKey.self] = newValue }
    }
}
