//
//  IRTextEnvironmentVariables.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

private struct IRTextColorKey: EnvironmentKey {
    static var defaultValue: Color? = nil
}

private struct IRTextStyleKey: EnvironmentKey {
    static var defaultValue: IRTextStyle = IRTextStyle(size: 16, color: Color("Primary"))
}

public extension EnvironmentValues {
    var irTextStyle: IRTextStyle {
        get { self[IRTextStyleKey.self] }
        set { self[IRTextStyleKey.self] = newValue }
    }
}

public extension EnvironmentValues {
    var irTextColor: Color? {
        get { self[IRTextColorKey.self] }
        set { self[IRTextColorKey.self] = newValue }
    }
}
