//
//  IRCheckbox.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

struct IRCheckboxIndeterminateEnabledKey: EnvironmentKey {
    static var defaultValue: Bool = false
}

public extension EnvironmentValues {
    var irCheckboxIndeterminateEnabled: Bool {
        get { self[IRCheckboxIndeterminateEnabledKey.self] }
        set { self[IRCheckboxIndeterminateEnabledKey.self] = newValue }
    }
}
