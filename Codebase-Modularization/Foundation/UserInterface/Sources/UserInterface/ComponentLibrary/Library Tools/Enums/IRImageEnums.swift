//
//  IRImageEnums.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public enum IRImageRatio: CaseIterable, CustomStringConvertible, Codable {
    case sixteenNine // 16:9
    case fourThree // 4:3
    case threeTwo // 3:2
    case oneone // 1:1
    case nineSixteen // 9:16
    case threeFour // 3:4
    case twoThree // 2:3
    case original // maintain the original aspect ratio
    
    var value: CGFloat {
        switch self {
        case .sixteenNine:
            return 16 / 9
        case .fourThree:
            return 4 / 3
        case .threeTwo:
            return 3 / 2
        case .oneone:
            return 1
        case .nineSixteen:
            return 9 / 16
        case .threeFour:
            return 3 / 4
        case .twoThree:
            return 2 / 3
        case .original:
            return 1
        }
    }
    
    public var description: String {
        switch self {
        case .sixteenNine:
            return "16:9"
        case .fourThree:
            return "4:3"
        case .threeTwo:
            return "3:2"
        case .oneone:
            return "1:1"
        case .nineSixteen:
            return "9:16"
        case .threeFour:
            return "3:4"
        case .twoThree:
            return "2:3"
        case .original:
            return "Original"
        }
    }
}

public enum IRImageSize: Codable {
    case small
    case medium
    case large
    case xlarge
    
    var value: CGFloat {
        switch self {
        case .small:
            return 75
        case .medium:
            return 100
        case .large:
            return 200
        case .xlarge:
            return 300
        }
    }
}
