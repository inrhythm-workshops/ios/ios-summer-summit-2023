//
//  IRText.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

public enum IRTextModifier: Hashable, CustomStringConvertible {
    case Bold
    case Italic
    case Monospaced
    case Underline
    
    public var description: String {
        switch self {
        case .Bold:
            return "Bold"
        case .Italic:
            return "Italic"
        case .Monospaced:
            return "Monospaced"
        case .Underline:
            return "Underline"
        }
    }
}
