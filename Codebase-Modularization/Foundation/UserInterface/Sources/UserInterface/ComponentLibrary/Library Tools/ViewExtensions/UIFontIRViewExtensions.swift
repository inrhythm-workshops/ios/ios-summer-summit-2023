//
//  UIFont.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

public extension View {
    func irFont(_ relativeTo: Font.TextStyle = .body) -> some View {
        UIFont.registerFontsIfNeeded()
        return self.font(.custom(getFontFamily(), size: 16, relativeTo: relativeTo))
    }
}

public extension UIFont {
    private static var otfFontsRegistered: Bool = false
    private static var ttfFontsRegistered: Bool = false
    
    static func registerFontsIfNeeded() {
        registerOTFFonts()
        registerTTFFonts()
    }
    
    static func listFonts() {
        for family in UIFont.familyNames.sorted() {
            let names = UIFont.fontNames(forFamilyName: family)
            //print("Family: \(family) Font names: \(names)")
        }
    }
    
    static func registerOTFFonts() {
        guard !otfFontsRegistered,
              let otfFontURLS: [URL] = IRUtility.getBundle(IRUtility.self)?
            .urls(forResourcesWithExtension: "otf", subdirectory: nil) else { return }
        otfFontURLS.forEach({
            CTFontManagerRegisterFontsForURL($0 as CFURL, .process, nil)})
        
        otfFontsRegistered = true
    }
    
    static func registerTTFFonts() {
        guard !ttfFontsRegistered,
              let otfFontURLS: [URL] = IRUtility.getBundle(IRUtility.self)?
            .urls(forResourcesWithExtension: "ttf", subdirectory: nil) else { return }
        otfFontURLS.forEach({
            CTFontManagerRegisterFontsForURL($0 as CFURL, .process, nil)})

        ttfFontsRegistered = true
    }
}
