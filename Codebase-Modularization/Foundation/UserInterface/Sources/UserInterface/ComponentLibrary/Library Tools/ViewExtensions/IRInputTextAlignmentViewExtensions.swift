//
//  IRInputTextAlignmentViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public extension View {
    func irInputTextAlignment(_ alignment: Alignment) -> some View {
        environment(\.irInputTextAlignment, (alignment, alignment == .leading ? TextAlignment.leading : TextAlignment.trailing))
    }
}
