//
//  View.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

public extension View {
    @ViewBuilder func `if`<Content: View>(_ condition: Bool, transform: (Self) -> Content) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }
    
    func irBorder() -> some View {
        self.padding()
            .overlay(RoundedRectangle(cornerRadius: 8).stroke(Color("IRBrand"), lineWidth: 0.5))
    }
    
    func irCard() -> some View {
        self.irBorder()
            .background(Color("Gray.UltraLight").opacity(0.2))
            
    }
}


