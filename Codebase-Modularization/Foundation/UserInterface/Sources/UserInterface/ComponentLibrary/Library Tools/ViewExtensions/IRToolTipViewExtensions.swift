//
//  IRToolTipViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

public extension View {
    func irTooltip(_ action: @escaping () -> Void) -> some View {
        environment(\.irTooltip, action)
    }
}
