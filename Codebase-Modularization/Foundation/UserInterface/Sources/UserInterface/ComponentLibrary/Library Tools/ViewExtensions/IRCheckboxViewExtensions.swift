//
//  IRCheckbox.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

public extension View {
    func irCheckboxIndeterminateEnabled(_ enabled: Bool = true) -> some View {
        environment(\.irCheckboxIndeterminateEnabled, enabled)
    }
}
