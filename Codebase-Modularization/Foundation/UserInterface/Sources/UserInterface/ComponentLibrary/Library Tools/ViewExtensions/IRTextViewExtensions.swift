//
//  IRText.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

public extension View {
    func irTextStyle(_ style: IRTextStyle) -> some View {
        environment(\.irTextStyle, style)
    }
}

public extension View {
    func irTextColor(_ color: Color) -> some View {
        environment(\.irTextColor, color)
    }
}


