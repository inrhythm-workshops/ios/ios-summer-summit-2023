//
//  IRButtonViewExtensions.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

public extension View {
    func IRButtonSize(_ size: IRButtonSize) -> some View {
        environment(\.irButtonSizeConfig, size)
    }
    
    func IRButtonWidth(_ widthType: IRButtonWidth) -> some View {
        environment(\.irButtonWidthConfig, widthType)
    }
    
    func IRButtonType(_ type: IRButtonType) -> some View {
        environment(\.irButtonColorConfig, .init(type: type))
    }
    
    func IRButtonIconAlignment(_ alignment: IRButtonIconAlignment = .leading) -> some View {
        environment(\.irButtonIconAlignment, alignment)
    }
}
