//
//  SingleLineRowWithDividers.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

struct SingleLineRowWithDividers: View {
    
    var sections: [String] = [String]()
    @State var sectionWidth: CGFloat = .zero
    @State var viewWidth: CGFloat = .zero
    init(sections: [String]) {
        self.sections = sections
    }
    
    var body: some View {
        ZStack {
            Color.clear.frame(maxWidth: UIScreen.main.bounds.width * 0.8, maxHeight: 0.1)
                .readSize { size in
                    print("View Width: \(size.width)")
                    viewWidth = size.width * 0.95
                }
            HStack {
                ForEach(sections.indices, id: \.self) { index in
                    HStack {
                        VStack(alignment: .leading) {
                            IRText(sections[index])
                                .frame(alignment: .topLeading)
                            Spacer()
                        }
                    }
                    .frame(alignment: .topLeading)
                    .frame(minWidth: ((viewWidth / Double(sections.count)) * 0.90), idealWidth: ((viewWidth / Double(sections.count)) * 0.90), maxWidth: (viewWidth / Double(sections.count) * 0.90), minHeight: 20, idealHeight: 20, maxHeight: 20)
                    if sections[index] != sections.last {
                        Divider()
                            .frame(width: 0.5, height: 20)
                            .overlay(Color("IRBrand"))
                            .padding(.top, -5)
                            .padding(.bottom, -5)
                    }
                }
            }.readSize { size in
                print(size.width)
            }
            .irCard()
        }
    }
}

struct SingleLineRowWithDividers_Previews: PreviewProvider {
    static var previews: some View {
        SingleLineRowWithDividers(sections: [
        "Item:",
        "Quantity:",
        "Price:"
        ])
    }
}
