//
//  IRToggleStyle.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

struct IRCheckboxToggleStyle: ToggleStyle {
    private let label: String
    private let value: String?
    private let sublabel: String?
    private let subvalue: String?
    private let tag: String
    private let isEnabled: Bool
    @Binding var selection: [String]
    @Binding var allTags: [String]
    
    init(label: String, value: String?, sublabel: String?, subvalue: String?, tag: String, selection: Binding<[String]>, allTags: Binding<[String]>, isEnabled: Bool) {
        self.label = label
        self.value = value
        self.sublabel = sublabel
        self.subvalue = subvalue
        self.tag = tag
        self._selection = selection
        self._allTags = allTags
        self.isEnabled = isEnabled
    }
    
    public func makeBody(configuration: Configuration) -> some View {
        Button(action: {configuration.isOn.toggle()}, label: {
            IRCheckboxRow(label: label, value: value, sublabel: sublabel, subvalue: subvalue, selection: $selection, tag: tag, allTags: $allTags, isOn: configuration.$isOn) })
        .onAppear(perform: {
            allTags.append(tag)
        })
        .onChange(of: configuration.isOn, perform: { _ in
            if selection.contains(tag) {
                selection.removeAll(where: { $0 == tag })
                configuration.isOn = false
            } else {
                selection.append(tag)
                configuration.isOn = true
            }
        })
        .accessibilityHidden(true)
    }
}

                          
