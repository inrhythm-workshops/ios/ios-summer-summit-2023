//
//  IRIconButtonStyle.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/5/23.
//

import SwiftUI

public struct IRIconButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.irButtonColorConfig) private var style: IRButtonColor
    @State var shape: IRIconButtonShape
    @State private var hovering: Bool = false
    
    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .background({ getBackgroundColor(configuration: configuration) }())
            .foregroundColor({ getForegroundColor(configuration: configuration) }())
            .clipShape( shape == .circle ? RoundedRectangle(cornerRadius: 100) : RoundedRectangle(cornerRadius: 8))
            .overlay(AnyView(RoundedRectangle(cornerRadius: 8).stroke(getBorderColor(configuration: configuration), lineWidth: style.strokeWidth)))
            .onHover { hoverState in
                hovering = hoverState
            }
    }
    
    func getBorderColor(configuration: ButtonStyleConfiguration) -> Color {
        if !isEnabled {
            return style.disabledBorderColor
        } else if configuration.isPressed {
            return style.activeBorderColor
        } else if hovering {
            return style.hoverBorderColor
        } else {
            return style.borderColor
        }
    }
    
    
    func getBackgroundColor(configuration: ButtonStyleConfiguration) -> Color {
        if !isEnabled {
            return style.disabledBackgroundColor
        } else if configuration.isPressed {
            return style.activeBackgroundColor
        } else if hovering {
            return style.hoverBackgroundColor
        } else {
            return style.backgroundColor
        }
    }
    
    func getForegroundColor(configuration: ButtonStyleConfiguration) -> Color {
        if !isEnabled {
            return style.disabledForegroundColor
        } else if configuration.isPressed {
            return style.activeForegroundColor
        } else if hovering {
            return style.hoverForegroundColor }
        else {
            return style.foregroundColor
        }
    }
}

