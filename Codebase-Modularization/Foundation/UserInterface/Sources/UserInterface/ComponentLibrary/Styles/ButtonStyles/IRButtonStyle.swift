//
//  IRButtonStyle.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 8/29/23.
//

import SwiftUI

public struct IRButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) private var isEnabled: Bool
    @Environment(\.irButtonColorConfig) private var style: IRButtonColor
    
    @State private var hovering: Bool = false
    
    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .background({getBackgroundColor(configuration: configuration)}())
            .foregroundColor({getForegroundColor(configuration: configuration)}())
            .overlay(AnyView(RoundedRectangle(cornerRadius: 8).stroke(getBorderColor(configuration: configuration), lineWidth: style.strokeWidth)))
            .onHover { hoverState in
                hovering = hoverState
            }
    }
    
    func getBorderColor(configuration: ButtonStyleConfiguration) -> Color {
        if !isEnabled {
            return style.disabledBorderColor
        } else if configuration.isPressed {
            return style.activeBorderColor
        } else if hovering {
            return style.hoverBorderColor
        } else {
            return style.borderColor
        }
    }
    
    
    func getBackgroundColor(configuration: ButtonStyleConfiguration) -> Color {
        if !isEnabled {
            return style.disabledBackgroundColor
        } else if configuration.isPressed {
            return style.activeBackgroundColor
        } else if hovering {
            return style.hoverBackgroundColor
        } else {
            return style.backgroundColor
        }
    }
    
    func getForegroundColor(configuration: ButtonStyleConfiguration) -> Color {
        if !isEnabled {
            return style.disabledForegroundColor
        } else if configuration.isPressed {
            return style.activeForegroundColor
        } else if hovering {
            return style.hoverForegroundColor }
        else {
            return style.foregroundColor
        }
    }
}


struct IRButton_style_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            IRButton(label: "Preview", action: {})
                .IRButtonType(.filled)

            IRButton(label: "Preview", action: {})
                .IRButtonType(.border)
        }
    }
}
