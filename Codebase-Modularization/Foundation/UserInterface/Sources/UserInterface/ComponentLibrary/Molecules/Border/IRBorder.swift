//
//  IRBorder.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

struct IRBorder: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    var isFocused: FocusState<Bool>.Binding
    @Binding var isError: Bool
    
    
    var body: some View {
        RoundedRectangle(cornerRadius: 8)
            .strokeBorder(borderColor, lineWidth: borderWidth)
    }
    private var borderColor: Color {
        guard isEnabled else { return Color("gray.light") }
        
        if isError { return .red }
        if isFocused.wrappedValue { return Color("IRBrand") }
        return .gray
    }
                
    private var borderWidth: CGFloat {
        guard isEnabled else { return 1 }
        if UIAccessibility.isVoiceOverRunning { return 1 }
            return isFocused.wrappedValue ? 2 : 1
        }
    }
