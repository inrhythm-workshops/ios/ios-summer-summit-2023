//
//  IRShimmer.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

struct IRFillShimmer: View {
@State var opacity: Double = 0.25

    var body: some View {
        Rectangle()
            .fill(Color("gray.light"))
            .opacity(opacity)
            .transition(.opacity)
            .onAppear {
                let shimmerAnimation = Animation.easeOut(duration: 2).repeatForever(autoreverses: true)
                withAnimation(shimmerAnimation) {
                    self.opacity = 0
                }
            }
    }
}
