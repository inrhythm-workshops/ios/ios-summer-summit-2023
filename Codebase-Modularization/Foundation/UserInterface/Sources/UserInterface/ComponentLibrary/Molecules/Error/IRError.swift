//
//  IRError.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/8/23.
//

import SwiftUI

struct IRErrorLabel: View {
    @Environment(\.isEnabled) private var isEnabled: Bool
    let errorMessage: String
    
    var body: some View {
        Label(errorMessage, systemImage: "exclamationmark.circle.fill")
            .irFont()
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundColor(isEnabled ? .red : Color("gray.light"))
            .accessibilityHidden(true)
            .padding(.top, 8)
    }
}
