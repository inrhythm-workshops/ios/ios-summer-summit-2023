//
//  ShoppingCartRow.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 7/29/23.
//


import SwiftUI
import ECOMUserInterface

public struct ShoppingCartRow: View {
    
    let item: DemoCartItem
    
    @State var width: CGFloat = 10
    
    public init(item: DemoCartItem) {
        self.item = item
    }
    
    public var body: some View {
        HStack {
            if let source = item.source {
                IRImage(source: Image(source), caption: "", ratio: .oneone, showCaption: false)
            }
            
            if let url = item.url {
                IRImage(url: url, caption: "", showCaption: false)
            }
            
            VStack(alignment: .leading) {
                IRText(item.label)
                    .irTextStyle(IRSystemTextStyles.body)
                IRText("\(item.sublabel), \(item.note)")
                    .irTextStyle(IRSystemTextStyles.contentNote)
                    
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            
            VStack(alignment: .center) {
                Text(String(item.quantity))
                    .frame(alignment: .center)
            }
            .frame(maxWidth: 1/8 * width)
            
            
            VStack(alignment: .center) {
                Text("$\(String(item.price))")
            }
            .frame(maxWidth: 1/4 * width)
            
            VStack(alignment: .center) {
                IRIconButton(iconName: "trash.fill", action: {}, shape: .circle)
                    .IRButtonType(.border)
            }
            .frame(maxWidth: 1/8 * width)
        }
        .readSize { size in
            width = size.width
        }
        .frame(maxWidth: .infinity,alignment: .leading)
    }
}

struct ShoppingCartRow_Previews: PreviewProvider {
    static var item: DemoCartItem = DemoCartItem(label: "Adidas Soccer Ball", sublabel: "Al Rihla World Cup", note: "Mini Soccer Ball", quantity: 1, price: 13.99, source: "Soccerball", url: nil)
    
    static var previews: some View {
        ShoppingCartRow(item: item)
    }
}
