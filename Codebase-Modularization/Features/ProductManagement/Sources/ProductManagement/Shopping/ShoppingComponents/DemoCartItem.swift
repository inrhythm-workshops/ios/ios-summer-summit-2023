//
//  ShoppingCartItem.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI



public struct DemoCartItem: ShoppingCartItem {
    public var id: UUID
    var label: String
    var sublabel: String
    var note: String
    var quantity: Int
    var price: Double
    var source: String?
    var url: String?
    
    public init(label: String, sublabel: String, note: String, quantity: Int, price: Double, source: String? = nil, url: String? = nil) {
        self.id = UUID()
        self.label = label
        self.sublabel = sublabel
        self.note = note
        self.quantity = quantity
        self.price = price
        self.source = source
        self.url = url
    }
}
