//
//  ShoppingDemoItems.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

class ShoppingDemoItems {
    static let soccerball: DemoCartItem = DemoCartItem(label: "Adidas Soccer Ball", sublabel: "Al Rihla World Cup", note: "Mini Soccer Ball", quantity: 1, price: 13.99, source: "Soccerball", url: nil)
    static let ipad: DemoCartItem = DemoCartItem(label: "10.9-inch iPad", sublabel: "64GB", note: "blue", quantity: 1, price: 399, source: "iPad")
}
