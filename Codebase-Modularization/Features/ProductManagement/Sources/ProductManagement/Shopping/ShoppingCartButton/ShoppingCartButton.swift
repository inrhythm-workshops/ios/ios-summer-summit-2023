//
//  shoppingCartButton.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 7/29/23.
//

import SwiftUI
import ECOMUserInterface

struct ShoppingCartButton: View {
    var action: () -> Void
    
    init(action: @escaping () -> Void) {
        self.action = action
    }
    
    var body: some View {
        IRIconButton(iconName: "cart.fill", action: {
            action()
        } , shape: .circle)
    }
}


struct ShoppingCartButton_Previews: PreviewProvider {
    static var previews: some View {
        ShoppingCartButton(action: {})
    }
}
