//
//  ShoppingCartItem.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 9/11/23.
//

import SwiftUI

protocol ShoppingCartItem: Hashable, Identifiable {
    var id: UUID { get set }
    var label: String { get set }
    var sublabel: String { get set }
    var note: String { get set }
    var quantity: Int { get set }
    var price: Double { get set }
    var source: String? { get set }
    var url: String? { get set }
}
