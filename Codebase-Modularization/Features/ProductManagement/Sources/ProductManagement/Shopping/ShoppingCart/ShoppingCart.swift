//
//  ShoppingCart.swift
//  inrhythm-ui-components
//
//  Created by Joshua Buchanan on 7/29/23.
//

import SwiftUI
import ECOMUserInterface

public struct ShoppingCart: View {
    
    @State var items: [DemoCartItem] = [ShoppingDemoItems.soccerball, ShoppingDemoItems.ipad]
    
    var subtotal: String {
        return String(Double(items.map({ $0.price }).reduce(0, +)))
    }
    
    var shipping: String {
        return String(Double(items.count) * 4.99)
    }
    
    var total: String {
        if let subtotal = Double(subtotal) {
            if let shipping = Double(shipping) {
                return String (subtotal + shipping)
            }
        }
        return "??"
        
    }
    public init(items: [DemoCartItem] = [DemoCartItem]()) {
        self.items = items
    }
    
    public var body: some View {
        ScrollView {
            HStack(alignment: .top) {
                if #available(iOS 16.0, *) {
                    IRText("Shopping Cart")
                        .irTextStyle(IRSystemTextStyles.largeTitle)
                        .bold()
                        .padding(.leading, 2)
                        .padding(.trailing, 2)
                        .frame(alignment: .leading)
                } else {
                    // Fallback on earlier versions
                }
                Spacer()
            }
            VStack {
                SingleLineRowWithDividers(sections: [
                "Item:",
                "Quanitity:",
                "Price:"])
                
                ForEach(items, id: \.id) { item in
                    ShoppingCartRow(item: item)
                    
                    if item != items.last {
                        Divider()
                    }
                }
            }
            .irBorder()
            
            // Should be broken out into separate "pricing area" view
            VStack {
                HStack {
                    Text("Subtotal:")
                    Spacer()
                    Text(subtotal)
                }
                HStack {
                    Text("Shipping:")
                    Spacer()
                    Text(shipping)
                }
                HStack {
                    Text("Total:")
                    Spacer()
                    Text(total)
                }
            }
            .padding(.top, 10)
            
            // should be broken out into own "checkout button" component
            HStack {
                IRButton(label: "Checkout", action: {})
                    .IRButtonWidth(.full)
            }
            .padding(.top, 5)
        }
        .frame(maxWidth: 300)
        .padding(.horizontal)
    }
}

struct MyPreviewProvider_Previews: PreviewProvider {
    static var items: [DemoCartItem] = [
        DemoCartItem(label: "thing 1", sublabel: "small", note: "discounted", quantity: 1, price: 0.99),
        DemoCartItem(label: "thing 2", sublabel: "medium", note: "purple", quantity: 2, price: 3.49),
        DemoCartItem(label: "thing 3", sublabel: "large", note: "fancy", quantity: 1, price: 999.99)]
    static var previews: some View {
        ShoppingCart(items: items)
    }
}
