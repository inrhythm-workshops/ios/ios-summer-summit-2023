//
//  LoginView.swift
//  Authentication
//
//  Created by Hamid Mahmood on 9/24/23.
//

import SwiftUI
import ECOMUserInterface


public struct LoginView: View {
    @State var username: String = ""
    @State var password: String = ""
    
    public init() {}
    
    public var body: some View {
        VStack {
            IRImage(source: Image("IRFull_Logo_White"), caption: "", ratio: .sixteenNine, showCaption: false)
                .frame(width: UIScreen.main.bounds.width / 2)
            VStack {
                IRInput(label: "Username", text: $username, placeholder: "Username")
                    .padding(.bottom, 20)
                IRInput(label: "Password", text: $password, placeholder: "Password")
                    .irTooltip {
                        print("toolTip tapped")
                    }
                    .padding(.bottom, 20)
            }
            .irBorder()
            
            IRButton(label: "Login", action: {})
                .IRButtonType(.border)
            
        }
        .padding(.horizontal)
    }
}

