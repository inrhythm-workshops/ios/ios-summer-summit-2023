Pod::Spec.new do |spec|
    spec.name          = 'Authentication'
    spec.version       = '1.0.0'
    spec.summary       = 'Authentication library'
    spec.license       = { :type => 'MIT', :file => 'License.md'}
    spec.homepage      = 'https://gitlab.com/'
    spec.source        = { :git => ' ', :tag => spec.version, :path => '/' }
    spec.author = {'Mahmood, Hamid' => 'hmahmood@inrhythm.com'}
    spec.module_name   = 'Authentication'
    spec.ios.deployment_target  = '15.0'
    spec.watchos.deployment_target  = '6'
    spec.swift_version = '5'
    spec.source_files       = 'Sources/Authentication/**/*.swift'
    spec.resources = [
      'Sources/Authentication/*.strings',
      'Sources/Authentication/*.{xcassets}',
    ]
    spec.dependency 'Networking'
    spec.dependency 'ECOMUserInterface'
  end