## iOS Summer Summit - InRhythm

The purpose of exercise is to engage audience to create an independent feature module/framework live during summit session and integrate into the host app.

### Frameworks/Modules
- Foundation
  - ECOMUserInterface
  - Networking
- Feature 
  - Authentication
  - ProductManagement
  - ShippingReturns
  - OrderManagement

 All frameworks/modules are already created except **OrderManagement** which needs to created and integrated into the host app (**EcommerceApp**)

### Steps to follow

```
git clone https://gitlab.com/inrhythm-workshops/ios/ios-summer-summit.git
cd ios-summer-summit
cd Codebase-Modularization
```

- Open Xcode 
- Start creating a new framework and name it **OrderManagement** under **Foundation** folder.
- Setup pod for the framework using ``` pod init ``` and ``` pod install```
- Create a **OderManagement.podspec** under **OrderManagement**
- You can simply take help from existing **.podspec** files and even resuse the existing scripts.
- Once you are setup with creating **OrderManagement** and **OderManagement.podspec**, start integrating the module into host app (**EcommerceApp**)
- Make sure to add **OrderManagement**  depedency in ```Podfile``` of host app.
- Do pod install under host app (**EcommerceApp**), build the app and you are all set.